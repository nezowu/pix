#define _XOPEN_SOURCE
#include <wchar.h>
#include <stdlib.h>
#include <string.h>

static int width(char *s, int flag) {
	wchar_t wc;
	mbtowc(&wc, s, flag);
	return wcwidth(wc);
}
size_t fmline_r(char *line, int pos, int *indent) { //задача нати размер в байтах по позиции и корректно обрезать
	*indent = 2;
//	int pot = pos;
	size_t siz = strlen(line);
	char *s = &line[siz-1];
	int i;
	size_t count = 0;
	int flag = 0;
	if(siz == 1)
		return 0; //выходим с нулем
	for (i=siz; i > 0; i--, s--) { //строка не обязана начинаться машинным нулем
		flag++;
		if((*s & 0xC0) != 0x80) {
			if(flag > 2)
				pos-=width(s, flag);
			else
				pos--;
			count+=flag; //считаем байты
			if(pos < 1)
				break;
			flag = 0;
		}
	}
	if(pos < 0)
		*indent+=pos; //1 или 2 смещение в позиции строки
	return siz - count; //возвращает смещение, стартовый индекс строки
}
size_t fmline_rev(char *line, int pos, int *add_format, int *indent) { //задача нати размер в байтах по позиции и корректно обрезать
	*indent = 2; //отступ
	int pot = pos;
	size_t siz = strlen(line);
	char *s = &line[siz-1];
	int i;
	size_t count = 0;
	int flag = 0;
	if(siz == 1)
		return 0; //выходим с нулем
	for (i=siz; i > 0; i--, s--) { //строка не обязана начинаться машинным нулем
		flag++;
		if((*s & 0xC0) != 0x80) {
			if(flag > 2)
				pos-=width(s, flag);
			else
				pos--;
			count+=flag; //считаем байты
			if(pos < 1)
				break;
			flag = 0;
		}
	}
	if(pos < 0) {
		*indent+=pos; //1 или 2 смещение в позиции строки
		*add_format = count - (pot+1);
	}
	else
		*add_format = count - (pot - pos);
	return siz - count; //возвращает смещение, стартовый индекс строки
}
size_t fmline(char *line, int pos, int *add_format) {
	int pot = pos;
	int len, wide, count = 0;
	char *s = line;
	for(;*s;) {
		if((*s & 0xC0) != 0x80) {
			len = mblen(s, MB_CUR_MAX); //исключить однобайтные символы
			if(len > 2) {
				wide = width(s, len);
				pos-=wide;
				if(pos < 0) {
					pos+=wide;
					break;
				}
			}
			else {
				pos-=1;
			}
			count+=len;
			s+=len;
			if(pos == 0)
				break;
		}
	}
	*add_format = count - (pot - pos); //+ к ширине поля для форматной строки
	return count;
}
