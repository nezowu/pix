#include <stdio.h>
#include <magic.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h>
#include <ctype.h>
#include <sys/wait.h>

#define TXT "text"	//#1
#define PDF "pdf"	//#2
#define DJVU "djvu"	//#3
#define VIDEO "video"	//#4
#define AUDIO "audio"	//#5
#define JPG "jpeg"	//#6 //скомпилировать регулярку?
#define PNG "png"		//#6
#define BMP "bmp"		//#6

static int term;
static char def[9][128] = {0};

static void read_conf(char def[][128]) { //вызывается из главной программы в начале работы-инициализации

	int i = 0;
	char *s;
	char buf[128];
	term = (strstr(getenv("TERM"), "linux"))? 0: 1; //переписать на strcmp
	if(term) {
		strcpy(buf, getenv("HOME")); //создать директорию pix и файл tabs//а если от рута?
		strcat(buf, "/.config/pix/pixrc"); //попробовать ~/
		FILE *file;
		if((file = fopen(buf, "r")) == NULL) { //проверить на ошибку
			if(access(buf, F_OK)) {
				perror("fopen"); //если конфига нет создать из скелетона
				exit(EXIT_FAILURE);
			}
		}
		else {
			while(fgets(buf, sizeof(buf), file) != NULL) {
				if(*buf == '#' || isspace(*buf))
					continue;
				if((s = strchr(buf, '=')) != NULL)
					strncpy(def[i++], s, strlen(++s)-1); //memcpy
				if(i == 8)
					break;
			}
			fclose(file);
		}
		s = strchr(def[0], ' ');
		*s = '\0';
		strcpy(def[8], ++s);
	}
	else {
	}
	printf("%s %s\n", def[0], def[8]);
}

static int case_mime(char *s) { // const char *s
	int num = 0;
	magic_t magic;
	const char *mime;
	magic = magic_open(MAGIC_MIME_TYPE); 
	magic_load(magic, NULL);
	//  magic_compile(magic, NULL);
	mime = magic_file(magic, s);
	if(strstr(mime, TXT))
		num = 1;
	else if(strstr(mime, PDF))
		num = 2;
	else if(strstr(mime, DJVU))
		num = 3;
	else if(strstr(mime, VIDEO))
		num = 4;
	else if(strstr(mime, AUDIO))
		num = 5;
	else if(strstr(mime, JPG))
		num = 6;
	else if(strstr(mime, PNG))
		num = 6;
	else if(strstr(mime, BMP))
		num = 6;

	printf("%s\n", mime);
	magic_close(magic);
	return num;
}

void launch(char *s) {
	int num;
	pid_t pid;
	read_conf(def);
	num = case_mime(s);
	if(!num)
		return; //linux пока не нужно
	if((pid = fork()) == 0) {
		if(num == 1 || num == 5) {
			execlp(def[0], def[0], def[8], def[num], s, NULL);
		}
		else {
			execlp(def[num], def[num], s, NULL);
		}
		exit(EXIT_SUCCESS);
	}
	else
		if(!term)
			wait(NULL);
	return;
}

int main(int argc, char **argv){
	launch(argv[1]);
//	pause();
//	printf("%s %s %s\n", def[num], basename(def[num]), argv[1]);
	return 0;
}
