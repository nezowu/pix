#define _XOPEN_SOURCE
#include <wchar.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>

int charwidth(char *s, int *size) {
	wchar_t wc;
	*size = 0;
	if((*(s+1) & 0xC0) != 0x80)
		*size = 3;
	else
		*size = 4;
	mbtowc(&wc, s-2, *size);
	return wcwidth(wc) - 1; //1 если символ двойной ширины
}

size_t bytesInPos(char *s, int pos, int *add_format) { //задача нати размер в байтах по позиции и корректно обрезать
	int post = pos;
	int size;
	int count = 0;
	int flag = 0;
	for (; *s; s++) {
		count++; //считаем байты
		flag++;
		if((*s & 0xC0) != 0x80) {
			flag = 0;
			pos--; //считаем позиции смволов одно и двух байтных
		}
		if(pos < 0) {
			if(flag == 1)
				count--;
			count--;
			break;
		}
		if(flag == 2) { //если мы досчитали до 3 байта, то пора проверить является ли символ широким?
			flag = 0;
			pos -= charwidth(s, &size); //если является то изменим позицию еще на единицу
			if(pos < 0) {
				count -= size;
				pos+=2;
				break;
			}
		}
	}
	*add_format = count - (post - pos); //смещение для форматной строки
	return count;
}

int charwidth_r(char *s, int *size) {
	wchar_t wc;
	*size = 0;
	if((*(s-1) & 0xC0) != 0x80) {
		*size = 3;
		mbtowc(&wc, s-1, *size);
	}
	else {
		*size = 4;
		mbtowc(&wc, s-2, *size);
	}
	return wcwidth(wc) - 1;
}

size_t bytes_in_pos_r(char *str, int pos, int *add_format) { //задача нати размер в байтах по позиции и корректно обрезать
	*add_format = ' '; //плюс один к переменной pos
	int i = (int)strlen(str)-1;
	char *s = &str[i];
	int size;
	int bytes = 0;
	int flag = 0;
	for (; i >= 0; i--, s--) { //строка не обязана начинаться машинным нулем
		bytes++; //считаем байты
		flag++;
		if((*s & 0xC0) != 0x80) {
			flag = 0;
			pos--; //считаем позиции смволов одно и двух байтных
		}
		if(pos < 1) {
			*add_format = '~';
			break;
		}
		if(flag == 2) { //если мы досчитали до 3 байта, то пора проверить является ли символ широким?
			flag = 0;
			pos -= charwidth_r(s, &size); //если является то изменим позицию еще на единицу
			if(pos < 1) {
				bytes-=2;
				*add_format = '~';
				break;
			}
		}
	}
	return bytes - flag;
}

int main() {
	setlocale(LC_ALL, "");
	int flag = 0;
//	char buf[] = "стоял стол и sтул_table";
//	char buf[] = "甲骨文周宣王甲骨文周𛀀𛀀宣王";
//	char buf[] = "私はあなたを愛し";
//	char buf[] = "༗ 𛀀𛀀ឦ 𐒣𐒁𛀀𛀀ឦ 𐒣𐒁𐌌𐌈𐏉 ";
	char buf[] = "𛀀sщ";
	int add_format;
	int i;
//	i = bytesInPos(buf, 10, &add_format);
//	buf[i] = '\0';
//	printf("%s %d\n", buf, i);

	i = bytes_in_pos_r(buf, 10, &add_format);
	printf("%c%s %d %zu\n", add_format, buf + (strlen(buf)-i), i, strlen(buf));
//	for(; *s; s--) {
//		count++;
//		if((*s & 0xC0) != 0x80) {
//			flag = 0;
//			pos--;
//		}
//	}
//	printf("%d\n", count);
}

