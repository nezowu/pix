#define _XOPEN_SOURCE
#include <wchar.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <limits.h>

int charwidth(char *s, int *size) {
	wchar_t wc;
	*size = 0;
	if((*(s+1) & 0xC0) != 0x80)
		*size = 3;
	else
		*size = 4;
	mbtowc(&wc, s-2, *size);
	return wcwidth(wc) - 1; //1 если символ двойной ширины
}

size_t bytesInPos(char *s, int pos, int *add_format) { //задача нати размер в байтах по позиции и корректно обрезать
	int post = pos;
	int size;
	int count = 0;
	int flag = 0;
	for (; *s; s++) {
		count++; //считаем байты
		flag++;
		if((*s & 0xC0) != 0x80) {
			flag = 0;
			pos--; //считаем позиции смволов одно и двух байтных
		}
		if(pos < 0) {
			if(flag == 1)
				count--;
			count--;
			break;
		}
		if(flag == 2) { //если мы досчитали до 3 байта, то пора проверить является ли символ широким?
			flag = 0;
			pos -= charwidth(s, &size); //если является то изменим позицию еще на единицу
			if(pos < 0) {
				count -= size;
				pos+=2;
				break;
			}
		}
	}
	*add_format = count - (post - pos); //смещение для форматной строки
	return count;
}

int charwidth_r(char *s, int *size) {
	wchar_t wc;
	*size = 0;
	if((*(s-1) & 0xC0) != 0x80) {
		*size = 3;
		mbtowc(&wc, s-1, *size);
	}
	else {
		*size = 4;
		mbtowc(&wc, s-2, *size);
	}
	return wcwidth(wc) - 1;
}

size_t bytes_in_pos_r(char *str, int pos, int *add_format) { //задача нати размер в байтах по позиции и корректно обрезать
	*add_format = ' '; //плюс один к переменной pos
	int i = (int)strlen(str)-1;
	char *s = &str[i];
	int size;
	int bytes = 0;
	int flag = 0;
	for (; i >= 0; i--, s--) { //строка не обязана начинаться машинным нулем
		bytes++; //считаем байты
		flag++;
		if((*s & 0xC0) != 0x80) {
			flag = 0;
			pos--; //считаем позиции смволов одно и двух байтных
		}
		if(pos < 1) {
			*add_format = '~';
			break;
		}
		if(flag == 2) { //если мы досчитали до 3 байта, то пора проверить является ли символ широким?
			flag = 0;
			pos -= charwidth_r(s, &size); //если является то изменим позицию еще на единицу
			if(pos < 1) {
				bytes-=2;
				*add_format = '~';
				break;
			}
		}
	}
	return bytes - flag;
}

int width(char *s, int flag) {
        wchar_t wc; 
        mbtowc(&wc, s, flag);
        return wcwidth(wc);
}
size_t bytes_r(char *line, int pos) { //задача нати размер в байтах по позиции и корректно обрезать
        size_t siz = strlen(line);
        char *s = &line[siz-1];
        int i;
        size_t bytes = 0;
        int flag = 0;
        if(siz == 1)
                return 0; //выходим с нулем

        for (i=siz; i > 0; i--, s--) { //строка не обязана начинаться машинным нулем
		int p;
                bytes++; //считаем байты
                flag++;
                if((*s & 0xC0) != 0x80) {
//                        if(flag > 2) {
				printf("%d\n", p = width(s, flag));
                   //             pos-=width(s, flag);
				pos-=p;
//			}
//                        else
//                                pos--;
                        flag = 0;
                        if(pos < 1)
                                break;
                }
        }
        return siz - bytes;
}
int main() {
	setlocale(LC_ALL, "");
//	size_t resalt;
//	int count = 0;
	char buf[] = "𐒣𐒁𛀀𐒣𐒁𐏉.𐌈.txt";
//	char buf[] = "стол";
//	int flag = 0;
//	char buf[] = "стоял стол и sтул_table";
//	char buf[] = "甲骨文周宣王甲骨文周𛀀𛀀宣王";
//	char buf[] = "私はあなたを愛し";
//	char buf[] = "༗ 𛀀𛀀ឦ 𐒣𐒁𛀀𛀀ឦ 𐒣𐒁𐌌𐌈𐏉 ";
//	char buf[] = "𛀀sщ";
//	int add_format;
//	int i;
//	i = bytesInPos(buf, 10, &add_format);
//	buf[i] = '\0';
//	printf("%s %d\n", buf, i);

//	i = bytes_in_pos_r(buf, 10, &add_format);
//	printf("%c%s %d %zu\n", add_format, buf + (strlen(buf)-i), i, strlen(buf));
//	for(; *s; s--) {
//		count++;
//		if((*s & 0xC0) != 0x80) {
//			flag = 0;
//			pos--;
//		}
//	}
//	printf("%d\n", count);
//	size_t siz = strlen(buf);
//	char *s = buf;
//	for(; *s; s++) {
//	       if((*s & 0xC0) != 0x80) {
//		       count++;	       
//	       }
//	}
//	printf("%d\n", count);
//	resalt = bytes_r(buf, 20);
//	printf("%s %zu\n", &buf[resalt], resalt);
//	printf("%d\n", MB_CUR_MAX);
	char *s = buf;
	int bytes = 0, width, len;
	wchar_t wc;
	while(*s) {
		bytes++;
		if((*s & 0xC0) != 0x80) {
			len = mblen(s, MB_CUR_MAX);
			printf("%d\t", len);
			if(len > 1) {
				mbtowc(&wc, s, len);
				width = wcwidth(wc);
			}
			s+=len;
			printf("%d\n", width);
			width = 1;
		}
	}
}
//байты функция(строка, диапазон, кол позиций в диапазоне + байты);
//байты функция(строка, диапазон); wclrtoeol(Win);

size_t fmline(char *line, int pos, int &add_format) {
	int pot = pos;
	int len, count = 0;
	wchar_t wc;
	char *s = line;
	for(;*s;) {
		if((*s & 0xC0) != 0x80) {
			len = mblen(s, MB_CUR_MAX);
			mbtowc(&wc, s, len);
			width = wcwidth(wc);
			if((pot-width) < 0)
				break;
			pot-=width;
			count+=len;
			s+=len;
			if(pot == 0)
				break;
		}
	}
	*add_format = count - (pos - pot); /0 или 1
	return count;
}
