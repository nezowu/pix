#include <stdio.h>
#include <magic.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h>
#include <ctype.h>
#include <sys/wait.h>
#include <curses.h>
#include <time.h>
//#include <curses.h>

#define TXT "text"	//#1
#define PDF "pdf"	//#2
#define DJVU "djvu"	//#3
#define VIDEO "video"	//#4
#define AUDIO "audio"	//#5
#define JPG "jpeg"	//#6 //скомпилировать регулярку?
#define PNG "png"		//#6
#define BMP "bmp"		//#6

//int static term; //static ?
char def[9][32] = {0}; //потом сделать static

char *strtime(time_t *ltime) {
	return ctime(ltime)+4;
}

int filline(void);

void read_conf(void) { //вызывается из главной программы в начале работы-инициализации
	time_t ltime;
	time(&ltime);
	int i = 0;
	char *s;
	char buf[128];
	FILE *file;
	freopen("/var/log/pix.log", "w", stderr);
	fprintf(stderr, "%s", strtime(&ltime));
	fflush(stderr);
	if(!(*getenv("TERM") - 'l'))
		perror("no support vtconsole"), exit(EXIT_FAILURE);
	strcpy(buf, getenv("HOME")); //создать директорию pix и файл tabs//а если от рута?
	strcat(buf, "/.config/pix"); //попробовать ~/#изменено с /root/.config/pix/pixrc
	if((file = fopen("/etc/pix/config", "r")) == NULL) { //проверить на ошибку
//		if(access(buf, F_OK)) {
			perror("config does not exist"); //если конфига нет создать из скелетона
			exit(EXIT_FAILURE);
//		}
	}
	else {
		while(fgets(buf, sizeof(buf), file) != NULL) {
			if(*buf == '#' || isspace(*buf))
				continue;
			if((s = strchr(buf, '=')) != NULL) {
				s++;
				memcpy(def[i++], s, strlen(s)-1);
			}
			if(i == 8)
				break;
		}
		fclose(file);
	}
	s = strchr(def[0], ' ');
	*s = '\0';
	strcpy(def[8], ++s);
//	fprintf(log, "%s %s\n", def[0], def[8]);
}

static int case_mime(char *s) { // const char *s
	int num = 0;
	magic_t magic;
	const char *mime;
	magic = magic_open(MAGIC_MIME_TYPE);
	magic_load(magic, NULL);
	//  magic_compile(magic, NULL);
	mime = magic_file(magic, s);
	if(strstr(mime, TXT))
		num = 1;
	else if(strstr(mime, PDF))
		num = 2;
	else if(strstr(mime, DJVU))
		num = 3;
	else if(strstr(mime, VIDEO))
		num = 4;
	else if(strstr(mime, AUDIO))
		num = 5;
	else if(strstr(mime, JPG))
		num = 6;
	else if(strstr(mime, PNG))
		num = 6;
	else if(strstr(mime, BMP))
		num = 6;

//	fprintf(stderr, "%s\n", mime);
	magic_close(magic);
	return num;
}

int launch(char *s) {
	int num;
	pid_t pid;
	num = case_mime(s);
	if(!num)
		return EXIT_FAILURE;
	if((pid = fork()) == -1)
		exit(EXIT_FAILURE);
	if(!pid) {
		if(num == 1 || num == 5) { //написать глобинг по /usr/share/applications
			execlp(def[0], def[0], def[8], def[num], s, NULL);
		}
		else {
			execlp(def[num], def[num], s, NULL);
		}
		exit(EXIT_SUCCESS);
	}
	return EXIT_SUCCESS;
}

int launch_with(char *line, char *s) {
	int lineln, stat = 0;
	pid_t pid;
	*line = ':';
	lineln = filline();
	if(line[2] != '\0') { //вывести line в файл magic и все засунуть в функцию
		pid = fork();
		if(!pid) {
			if(!strcmp(&line[1], "vim") || !strcmp(&line[1], "mocp"))
				stat = execlp(def[0], def[0], def[8], &line[1], s, NULL);
			else
				stat = execlp(&line[1], &line[1], s, NULL);
			exit(EXIT_SUCCESS);
		}
	}
	memset(line, 0, lineln);
	return stat; //запись в массив и далее в конфиг
}
