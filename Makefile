CFLAGS = -Wall -Wextra -Iinclude
LDFLAGS = -lncursesw -lmagic
SRC = $(wildcard *.c)
OBJ = $(SRC:.c=.o)
P = pix
$(P): $(OBJ)
	gcc $(CFLAGS) $^ -o $@ $(LDFLAGS)
	
.PHONY : clean, install, uninstall

clean :
	rm -f $(P) *~ .*~ .*.swp *.o

install : $(P)
	mkdir /etc/pix
	cp config /etc/pix
	touch /var/log/pix.log
	chmod 662 /var/log/pix.log
	cp $(P) /usr/bin

uninstall :
	rm /var/log/pix.log
	rm /usr/bin/$(P)
	rm -rf /etc/pix
