// консольный файловый пейджер pix.c wch.c hash.c ldir.c perm.c magic.c dir.h my.h
#include "my.h"
//char cmdline[256] = {0};
extern char def[9][32];
int launch_with(char *, char *);

int main() {
	read_conf();
	line[0] = '\0'; //достаточно обнулить первый байт
	CURS = 0;
	OFFSET = 0;
	flag = true; //поставить тип bool - убать в локальную
	iflag = true;
	char *current;
	char buf[SIZ];
	int key = 0;
	setlocale(LC_ALL, "");
	signal(SIGWINCH, sig_handler);
	signal(SIGINT, sig_handler);
	start_ncurses();
	entry = pwd(&RAW, ".", flag);
	entry_p = pwd(&PREV, "..", flag);
	if(RAW.ar_len < L1)
		MENULEN = RAW.ar_len;
	else
		MENULEN = L1;
	cadr();
	cadr_p();
	char digit[8];
	int ind = 0;
	while((key = getch()) != ERR) {
		if(isdigit(key)){
			if(ind > 7) //защита от переполнения
				ind = 0;
			if(key == '0' && !ind)
				continue;
			digit[ind++] = key;
			continue;
		}
		switch(key) {
			case 'j':
				if(RAW.ar_len > 1) {
					if (CURS < RAW.ar_len - 1) {
						CURS++;
						if (CURS > OFFSET + MENULEN - 1)
							OFFSET++;
						cadr();
					}
				}
				break;
			case 'k':
				if(RAW.ar_len > 1) {
					if (CURS) {
						CURS--;
						if (CURS < OFFSET)
							OFFSET--;
						cadr();
					}
				}
				break;
			case 'G':
				if(RAW.ar_len > 1) {
					CURS = RAW.ar_len-1;
					if(ind) {
						digit[ind] = '\0'; //обрезаем буфер, отсекая мусор
						ind = atoi(digit); //используем как временную переменную
						if(ind < RAW.ar_len)
							CURS = ind - 1;
					}
					OFFSET = 0;
					if(CURS > OFFSET + MENULEN-1)
						OFFSET = CURS - MENULEN+1;
					cadr();
				}
				break;
			case 'g':
				if(RAW.ar_len > 1) {
					CURS = 0;
					OFFSET = 0;
					cadr();
				}
				break;
			case 'h':
				if(getcwd(buf, SIZ) == NULL) {
					chdir(getenv("HOME"));
					current = get_hash(getenv("HOME"));
				}
				else {
					if(buf[1] == '\0') //что в массиве при ошибке?
						break;
					chdir("..");
					if(RAW.ar_len > 1) //было > 0
						set_hash(buf, RAW.ar[CURS]->d_name); //запишем или перезапишем базу
					current = basename(buf);
				}
				CURS = 0;
				OFFSET = 0;
				reset();
				entry = pwd(&RAW, ".", flag);
				reset_p();
				entry_p = pwd(&PREV, "..", flag);
				if(current != NULL) {
					for(int i = 0; i < RAW.ar_len; i++) {
						if(!strcmp(current, RAW.ar[i]->d_name)) {
							CURS = i;
							break;
						}
					}
				}
				if(RAW.ar_len < L1)
					MENULEN = RAW.ar_len;
				else
					MENULEN = L1;
				if(CURS > OFFSET + MENULEN-1)
					OFFSET = CURS - MENULEN+1;
				cadr();
				cadr_p();
				break;
			case 'l':
				if(RAW.ar_len) {
					if(RAW.ar[CURS]->d_type == DT_REG){
						endwin();
						if(launch(RAW.ar[CURS]->d_name)) {
							launch_with(line, RAW.ar[CURS]->d_name);
							status();
						}
						refresh();
					}
					else {
						if(ACCESS)
							break;
						if(chdir(RAW.ar[CURS]->d_name))
							chdir(getenv("HOME"));
						CURS = 0;
						OFFSET = 0;
						MENULEN = 0;
						reset();
						entry = pwd(&RAW, ".", flag);
						reset_p();
						entry_p = pwd(&PREV, "..", flag);
						if((current = get_hash(getcwd(buf, SIZ))) != NULL) {
							for(int i = 0; i < RAW.ar_len; i++) {
								if(!strcmp(current, RAW.ar[i]->d_name)) { //strcmp
									CURS = i;
									break;
								}
							}
						}
						if(RAW.ar_len < L1)
							MENULEN = RAW.ar_len;
						else
							MENULEN = L1;
						if(CURS > OFFSET + MENULEN-1)
							OFFSET = CURS - MENULEN+1;
						cadr();
						cadr_p();
					}
				}
				break;
			case 'i':
				iflag = (iflag)? false: true;
				status();
				break;
			case 'a':
				flag = (flag)? false: true;	
				[[fallthrough]];
			case 'r':
				current = strdup(RAW.ar[CURS]->d_name);
				OFFSET = 0;
				CURS = 0;
				reset();
				entry = pwd(&RAW, ".", flag);
				for(int i = 0; i < RAW.ar_len; i++) {
					if(!strcmp(current, RAW.ar[i]->d_name)) {
						CURS = i;
						break;
					}
				}
				if(RAW.ar_len < L1)
					MENULEN = RAW.ar_len;
				else
					MENULEN = L1;
				if(CURS > OFFSET + MENULEN-1)
					OFFSET = CURS - MENULEN+1;

				reset_p();
				entry_p = pwd(&PREV, "..", flag);
				cadr();
				cadr_p();
				free(current);
				break;
			case 's':
//				erase();
				endwin();
				pid_t pid = fork();
				if(!pid) {
					setenv("PS1", "[\\u@\033[01;37mpix\033[00m \\W]\\$ ", 1); 
					execlp(def[0], def[0], NULL);
					exit(EXIT_SUCCESS);
				}
				current = strdup(RAW.ar[CURS]->d_name);
				OFFSET = 0;
				CURS = 0;
				reset();
				entry = pwd(&RAW, ".", flag);
				for(int i = 0; i < RAW.ar_len; i++) {
					if(!strcmp(current, RAW.ar[i]->d_name)) {
						CURS = i;
						break;
					}
				}
				refresh();
//				C0 = COLS/2;
//				C1 = C0/2;
//				C2 = C0 - 2;
//				C4 = C1 - 2;
//				C5 = C1 + 1;
//				L1 = LINES-2;
//				wresize(Prev, L1, C4);
//				wresize(Raw, L1, C2);
//				wresize(Next, L1, C4);
//				mvwin(Raw, 1, C5);
//				mvwin(Next, 1, C0 + C5);
//				OFFSET = 0;
//				if(RAW.ar_len < L1)
//					MENULEN = RAW.ar_len;
//				else
//					MENULEN = L1;
//				if(CURS > OFFSET + MENULEN-1)
//					OFFSET = CURS - MENULEN+1;
//				refresh();
//				werase(Next);
				cadr();
				cadr_p();
				free(current);
				break;
			case 'q':
				endwin();
				exit(EXIT_SUCCESS);
				break;
			case 'n': //отдельная функция
			case 'N':
				break;
			case ':':
				if(RAW.ar_len) {
					if(RAW.ar[CURS]->d_type == DT_REG){ //new module
						endwin();
						launch_with(line, RAW.ar[CURS]->d_name);
						status();
						refresh();
					}
				}
				break;
			case '?':
			case '/':
				*line = (char)key;
				filline(); //следует функция которую еще придется написать
				memset(line, 0, 128);
				status();
				break;
//			case 10: //enter
//				endwin();
//				exit(EXIT_SUCCESS);
//			case [0-9]: если число - собираем в буфер
			default: //если не число и не знаки - очищаем буфер
				break;
		}
		ind = 0; //обнуляем индекс буфера
	}
//	fclose(log); //добавить в atexit
	return 0;
}
static void cadr_p() {
	werase(Prev);
	char buf[SIZ];
	size_t size;
	int i, j, add_format, offset = 0;

	char format_side[7];
	getcwd(buf, SIZ);
//	box(Prev, 0, 0);
	if(buf[1] == '\0') { //проверка директории на корень "/"
		sprintf(format_side, "%%-%ds", C4);
//		wattron(Prev, A_REVERSE | COLOR_PAIR(5));
		wattron(Prev, A_STANDOUT | COLOR_PAIR(5));
		wprintw(Prev, format_side, buf);
		wstandend(Prev);
	}
	else {
		for(i = 0; i < PREV.ar_len; i++) {
			if(!strcmp(PREV.ar[i]->d_name, basename(buf))) {
				if(i > LINES - 3)
				offset = i - (LINES - 3);
				break;
			}
		}
		for(j = offset; j < PREV.ar_len; j++) {
			size = fmline(PREV.ar[j]->d_name, C4, &add_format);
			memcpy(buf, PREV.ar[j]->d_name, size);
			buf[size] = '\0';
			sprintf(format_side, "%%-%ds", C4 + add_format);
			if(PREV.ar[j]->d_type == DT_DIR)
				wattron(Prev, COLOR_PAIR(5));
			else
				wattron(Prev, A_DIM);
			if(PREV.ar[j]->d_type == DT_LNK)
				wattron(Prev, COLOR_PAIR(2));
//			if(PREV.ar[j]->d_type == DT_REG) {
//				lstat(RAW.ar[j]->d_name, &sb);
//				if(sb.st_mode & S_IXOTH)
//					wattron(Prev, COLOR_PAIR(1));
//			}
			if(j == i)
				wattron(Prev, A_REVERSE);
			wprintw(Prev, format_side, buf);
			wstandend(Prev);
			if(j - offset == LINES - 3)
				break;
		}
	}
	wrefresh(Prev);
}
static void cadr(void) {
	werase(Raw);
	werase(Next);
	ACCESS = 1;
	char *current;
	int add_format;
	size_t temp;
	char buf[SIZ];
	int i, j;
	char format_raw[7], format_side[7]; //сделать орматную строку только под куросром!!!
	sprintf(format_side, "%%-%ds", C4); //и пропробовать не затирает ли соседнее окно на длинных файлах
//	box(Raw, 0, 0);
//	box(Next, 0, 0);
	entry_n = NULL; //?
	status();
	if(RAW.ar_len) { //главная колонка
		for(i = OFFSET, j = 0; j < MENULEN; i++, j++) {
			temp = fmline(RAW.ar[i]->d_name, C2, &add_format);
			sprintf(format_raw, "%%-%ds", C2 + add_format);
			memcpy(buf, RAW.ar[i]->d_name, temp);
			buf[temp] = '\0';
			if(RAW.ar[i]->d_type == DT_DIR)
				wattron(Raw, A_BOLD | COLOR_PAIR(5));
			if(RAW.ar[i]->d_type == DT_LNK)
				wattron(Raw, COLOR_PAIR(2));
			if(RAW.ar[i]->d_type == DT_REG) {
				if(access(RAW.ar[i]->d_name, X_OK) == F_OK)
					wattron(Raw, COLOR_PAIR(1));
			}
			if(i == CURS)
				wattron(Raw, A_REVERSE);
			wprintw(Raw, format_raw, buf);
			wstandend(Raw);
		}
		getcwd(buf, SIZ);
		if(RAW.ar[CURS]->d_type == DT_DIR ) { //выводим на экран третий столбец
			if(!access(RAW.ar[CURS]->d_name, R_OK)) {
				if(buf[1] != '\0')
					strcat(buf, "/");
				strcat(buf, RAW.ar[CURS]->d_name);
				ACCESS = 0;
				if(entry_n)
					reset_n();
				entry_n = pwd(&NEXT, RAW.ar[CURS]->d_name, flag);
				current = get_hash(buf); 
				for(i = 0; i < NEXT.ar_len; i++) {
					temp = fmline(NEXT.ar[i]->d_name, C4, &add_format);
					sprintf(format_side, "%%-%ds", C4 + add_format);
					memcpy(buf, NEXT.ar[i]->d_name, temp);
					buf[temp] = '\0';
					if(NEXT.ar[i]->d_type == DT_DIR)
						wattron(Next, COLOR_PAIR(5));
					else
						wattron(Next, A_DIM);
					if(current) {
					       	if(!strcmp(NEXT.ar[i]->d_name, current))
							wattron(Next, A_REVERSE);
					}
					else {
						if(i == 0)
							wattron(Next, A_REVERSE);
					}
					wprintw(Next, format_side, buf);
					wstandend(Next);
					if(i == LINES - 3)
						break;
				}
				if(!i) {
					memcpy(buf, "Empty", 6);
					if(C4 < 5 && C4 > 0)
						buf[C4] = '\0';
					else if(C4 < 1)
						buf[0] = '\0';
					wattron(Next, COLOR_PAIR(3));
					wprintw(Next, "%s", buf);
					wstandend(Next);
				}
			}
			else {
				ACCESS = 1;
				memcpy(buf, "Not accessible", 15);
				if(C4 < 14 && C4 > 0)
					buf[C4] = '\0';
				else if(C4 < 1)
					buf[0] = '\0';
				wattron(Next, COLOR_PAIR(3) | A_REVERSE | A_DIM);
				wprintw(Next, "%s", buf);
				wstandend(Next);
			}
		}
	}
	else {
		memcpy(buf, "Empty", 6);
		if(C2 < 5 && C2 > 0)
			buf[C2] = '\0';
		else if(C2 < 1)
			buf[0] = '\0';
		wattron(Raw, COLOR_PAIR(3));
		wprintw(Raw, "%s", buf);
		wstandend(Raw);
	}
	wrefresh(Raw);
	wrefresh(Next);
}
static void start_ncurses(void) {
	initscr();
	cbreak(); //raw();
	noecho();
	curs_set(FALSE);
//	erase();
	clear();
	L1 = LINES - 2;
	C0 = COLS/2;
	C1 = C0/2;
	C2 = C0 - 2;
	C4 = C1 - 2;
	C5 = C1 + 1;
	start_color();
	init_pair(1,COLOR_GREEN,0);
	init_pair(2,COLOR_CYAN,0);
	init_pair(3,COLOR_RED,0);
	init_pair(4,COLOR_YELLOW,0);
	init_pair(5,COLOR_BLUE,0);
	init_pair(6,COLOR_MAGENTA,0);
	init_pair(7,COLOR_WHITE,0);
	Prev = newwin(L1, C4, 1, 1);
	Raw = newwin(L1, C2, 1, C5);
	Next = newwin(L1, C4, 1, C0 + C5);
	refresh();
}
static void sig_handler(int signo) {
	if(signo == SIGWINCH) {
		endwin();
//		doupdate(); //	clearok(stdscr, TRUE);clear();refresh();
		refresh();
		L1 = LINES-2;
		C0 = COLS/2;
		C1 = C0/2;
		C2 = C0 - 2;
		C4 = C1 - 2;
		C5 = C1 + 1;
		wresize(Prev, L1, C4);
		wresize(Raw, L1, C2);
		wresize(Next, L1, C4);
		mvwin(Raw, 1, C5);
		mvwin(Next, 1, C0 + C5);
		OFFSET = 0;
		if(RAW.ar_len < L1)
			MENULEN = RAW.ar_len;
		else
			MENULEN = L1;
		if(CURS > OFFSET + MENULEN-1)
			OFFSET = CURS - MENULEN+1;
//		werase(Next);
		erase(); //clear()
		wnoutrefresh(stdscr);
//		doupdate();
		cadr();
		cadr_p();
		refresh();
	}
	else if(signo == SIGINT) {
//END_PROG: //написать atexit
		delwin(Prev);
		delwin(Raw);
		delwin(Next);
		endwin();
		fprintf(stderr, "%s\n", "Good bye! See you!");
		exit(EXIT_FAILURE);
	}
}
void reset(void) {
	for(int i = 0; i < RAW.len; i++) {
		free(entry[i]);
	}
	free(entry);
	free(RAW.ar);
}
void reset_p(void) {
	for(int i = 0; i < PREV.len; i++) {
		free(entry_p[i]);
	}
	free(entry_p);
	free(PREV.ar);
}
void reset_n(void) {
	for(int i = 0; i < NEXT.len; i++) {
		free(entry_n[i]);
	}
	free(entry_n);
	free(NEXT.ar);
}
static void navigate(void) {
	char buf[SIZ];
	int offset, add_format, indent;
	char format[7];
	char *zn = "~ ";
	if(iflag) {
		getcwd(buf, SIZ);
		offset = fmline_r(buf, COLS-3, &indent);
		if(offset < 1) {
			mvaddch(0, 0, ' ');
			mvaddstr(0, 1, buf); //глобальная переменная!
			clrtoeol();
		}
		else { //безопасный вывод неизвестной строки только через спецификатор %s
			mvaddstr(0, 0, zn);
			mvprintw(0, indent, "%s", &buf[offset]); //глобальная переменная!
		}
	}
	else {
		if(RAW.ar_len) {
			offset = fmline_rev(RAW.ar[CURS]->d_name, COLS-3, &add_format, &indent);
			sprintf(format, "%%%ds", COLS-3 + add_format);
			if(offset < 1) {
				mvaddstr(0, 0, "  ");
				mvprintw(0, indent, format, RAW.ar[CURS]->d_name); //глобальная переменная!
			}
			else { //безопасный вывод неизвестной строки только через спецификатор %s
				mvaddstr(0, 0, zn);
				mvprintw(0, indent, format, &RAW.ar[CURS]->d_name[offset]); //глобальная переменная!
			}
		}
		else {
			move(0, 0);
			clrtoeol();
		}
	}
}
static void status(void) { //вывод статуса и строки навигации
	navigate();
	struct stat st;
	struct passwd *psw;
	struct group *grp;
	char size[10]; //для функции human_read
	char format[7];
	int temp, quot;
	char buf[SIZ];
	if(*line) { //если строка поиска не пустая
		prinline(); //строка поиска
	}
	else {
		if(RAW.ar_len) {
			if(lstat(RAW.ar[CURS]->d_name, &st)) {
				crash();
				lstat(RAW.ar[CURS]->d_name, &st);
			}
			psw = getpwuid(st.st_uid);
			grp = getgrgid(st.st_gid);
			sprintf(buf, "%s %3lu %s:%s %s %s", lsperm(st.st_mode), \
					st.st_nlink, psw->pw_name, grp->gr_name, \
					human_read(st.st_size, size), strtime((time_t *)&st.st_mtim));
			temp = strlen(buf);
			if(temp > COLS-1) //если строка длиннее ширины поля
				buf[COLS-1] = '\0';
			mvprintw(LINES-1, 0, "%s", buf); //строка статуса

			sprintf(buf, " %3d/%-3d %3d%%  %3d;%-3d", CURS+1, RAW.ar_len, \
					(int)((CURS+1)/(float)RAW.ar_len*100), RAW.dir, RAW.ar_len - RAW.dir);
			quot = COLS - temp;
			temp = strlen(buf);
			if(temp > quot)
				buf[quot] = '\0';
			sprintf(format, "%%%ds", quot);
			printw(format, buf);
		}
		else {
			move(LINES-1, 0);
			clrtoeol();
		}
	}
//	navigate();
}
int filline() {
	int i, key;
	mvaddch(LINES-1, 0, *line);
	mvaddch(LINES-1, 1, ' ');
	clrtoeol();
	curs_set(TRUE);
	i = 1;
	while((key = getch()) != ERR) { //запретить вставку мышей?
		if(key == 27) { //esc заворачиваем esc последовательности
			nodelay(stdscr, TRUE);
			if(getch() == ERR) { //отлавливаем одиночный esc
				nodelay(stdscr, FALSE);
				break;
			}
			getch(); //очищаем буфер от остатков ескейп последовательностей
			getch();
			getch();
			getch();
			getch();
			nodelay(stdscr, FALSE); //возвращаемся в бокирующий режим
			continue;
		}
		if(key == 10) { //enter
//			memcpy(cmdline, &line[1], strlen(line)-1); 
			curs_set(FALSE);
			return i; //выходим сохраняя массив line не очищенным; нужна доп очистка
		}
		if(key < 27 || key == 410) //убираем управляющие символы и мусор
			continue;
		if(key == 127) { //'\177' код DEL нажата BS
			if(i == 1) {
				break;
			}
			int schet = 0;
			size_t siz = strlen(line);
			char *s = &line[siz]; //завершающий '\0'
			while(--s) { //проверка высше i == 1
				schet++;
				if((*s & 0xC0) != 0x80)
					break;
			}
			i-=schet;
			memset(&line[i], 0, schet);
			prinline();
			clrtoeol();
			continue;
		}
		line[i++] = key;
		nodelay(stdscr, TRUE);
		if((key = getch()) != ERR) {
			line[i++] = key;
			if((key = getch()) != ERR) {
				line[i++] = key;
				if((key = getch()) != ERR) {
					line[i++] = key;
					if((key = getch()) != ERR) {
						line[i++] = key;
						if((key = getch()) != ERR)
							line[i++] = key; //заменить на break
					}
				}
			}
		}
		nodelay(stdscr, FALSE);
		prinline();
		clrtoeol();
		if(i > 127) {
			break;
		}
	}
	curs_set(FALSE);
	memset(line, 0, i);
	status();
	return 0;
}
static void prinline() {
	char *zn = "~ ";
	int indent;
	int offset = fmline_r(line, COLS-3, &indent);
	if(offset < 2) {
		mvaddch(LINES-1, 0, *line);
		mvprintw(LINES-1, indent, "%s", &line[1]); //глобальная переменная!
	}
	else { //безопасный вывод неизвестной строки только через спецификатор %s
		mvaddstr(LINES-1, 0, zn);
		mvprintw(LINES-1, indent, "%s", &line[offset]); //глобальная переменная!
	}
}
static void crash() {
	char buf[SIZ];
	char *current;
	OFFSET = 0;
	if(getcwd(buf, SIZ) == NULL) {
		chdir(getenv("HOME"));
		current = get_hash(getenv("HOME"));
//		if(current == NULL)
		CURS = 0;
	}
	else {
		current = NULL;
		if(CURS > 0)
			CURS--;
	}
	reset();
	entry = pwd(&RAW, ".", flag);
	reset_p();
	entry_p = pwd(&PREV, "..", flag);
	if(current != NULL) {
		for(int i = 0; i < RAW.ar_len; i++) {
			if(!strcmp(current, RAW.ar[i]->d_name)) {
				CURS = i;
				break;
			}
		}
	}
	if(RAW.ar_len < L1)
		MENULEN = RAW.ar_len;
	else
		MENULEN = L1;
	if(CURS > OFFSET + MENULEN-1)
		OFFSET = CURS - MENULEN+1;
	cadr_p();
}
