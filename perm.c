#include <sys/stat.h>
#include <string.h>
#include <stdio.h>

static int unit(int mode) {
	char c;
	switch(mode & S_IFMT) {
		case S_IFREG:
			c = '-';
			break;
		case S_IFDIR:
			c = 'd';
			break;
		case S_IFCHR:
			c = 'c';
			break;
		case S_IFBLK:
			c = 'b';
			break;
		case S_IFLNK:
			c = 'l';
			break;
		case S_IFIFO:
			c = 'p';
			break;
		case S_IFSOCK:
			c = 's';
			break;
		default:
			c = '?';
	}
	return c;
}
char *lsperm(int mode) {
	static const char *rwx[] = {"---", "--x", "-w-", "-wx", "r--", "r-x", "rw-", "rwx"};
	static char bit[11];
	bit[0] = unit(mode);
	strcpy(&bit[1], rwx[(mode >> 6)& 7]);
	strcpy(&bit[4], rwx[(mode >> 3)& 7]);
	strcpy(&bit[7], rwx[(mode & 7)]);
	if (mode & S_ISUID)
		bit[3] = (mode & S_IXUSR) ? 's' : 'S';
	if (mode & S_ISGID)
		bit[6] = (mode & S_IXGRP) ? 's' : 'l';
	if (mode & S_ISVTX)
		bit[9] = (mode & S_IXOTH) ? 't' : 'T';
	bit[10] = '\0';
	return bit;
}
char* human_read(double size, char *buf) {
    int i = 0;
    const char* value[] = {" ", "K", "M", "G", "T"};
    while (size > 1024) {
        size /= 1024;
        i++;
    }
    sprintf(buf, "%6.*f%s", i, size, value[i]);
    return buf;
}
