#include "dir.h"
#include <curses.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <strings.h>
#include <locale.h>
#include <wchar.h>
#include <libgen.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdint.h>
#include <wait.h>
#include <ctype.h>
#include <pwd.h>
#include <grp.h>

//#define ERROR() {perror(NULL); goto END_PROG;} //нужно вести лог
//#define END() goto END_PROG;
#define SIZ 256

int CURS, OFFSET, MENULEN, ACCESS;
int L1, L3, C0, C1, C2, C4, C5;
WINDOW *Prev, *Raw, *Next;
Col PREV, RAW, NEXT;
bool flag, iflag;
char line[128];
struct dirent **entry, **entry_p, **entry_n;
//void init_hash(void);
size_t fmline(char *, int, int *);
size_t fmline_r(char *, int, int *);
size_t fmline_rev(char *, int, int *, int *);
static void cadr_p(void);
static void cadr(void);
static void sig_handler(int);
static void start_ncurses(void);
struct dirent ** pwd(struct col *, char *, bool);
//void atime(char *);
char * set_hash(char *, char *);
char * get_hash(char *);
void reset(void);
void reset_p(void);
void reset_n(void);
static void navigate(void);
static void status(void);
char *lsperm(int);
char* human_read(double, char *);
int filline(void);
static void prinline(void);
static void crash(void);
void read_conf(void);
int launch(char *);
char *strtime(time_t *);
